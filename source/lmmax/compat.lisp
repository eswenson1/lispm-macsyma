;; -*- Mode:LISP; Package:MACSYMA; Base:10 -*-

;; Maclisp compatibility definitions for the Lisp Machine.  This file
;; is for Lisp differences only.  No knowledge of Macsyma should be
;; contained in this file.

;; Translated code still uses this.
(DEFUN INCLUDEF (&QUOTE FILENAME) FILENAME)

;; Couldn't this copy to some static area?
(DEFMACRO PURCOPY (X) X)

(DEFMACRO CHARPOS (IGNORE) '(CDR (CURSORPOS)))

#-LISPM
(DEFMACRO SLEEP (SECONDS) `(PROCESS-SLEEP (FIX (* ,SECONDS 60.))))

(DEFUN LINEL (&OPTIONAL STREAM)
  (FUNCALL (SI:DECODE-PRINT-ARG STREAM) ':SIZE-IN-CHARACTERS))

;; Perhaps this should do something.
(DEFMACRO NOINTERRUPT (IGNORE) NIL)

(DEFMACRO WITHOUT-TTY-INTERRUPTS (&REST FORM)
  `(LET ((TV:KBD-INTERCEPTED-CHARACTERS NIL))
     ,@ FORM))

(DEFMACRO FIXNUM-IDENTITY (X) X)
(DEFMACRO FLONUM-IDENTITY (X) X)

#+3600
(DEFSUBST *QUO (X Y) (// X Y))
#+3600
(DEFSUBST *DIF (X Y) (- X Y))


;; Keep the compiler quiet.
;; Use GET-PNAME or FORMAT instead of EXPLODE, EXPLODEN, EXPLODEC.
;; Use AREF instead of GETCHAR and GETCHARN.
;; Use MAKE-SYMBOL instead of MAKNAM.
;; Use INTERN instead of IMPLODE.
;; Use STRING-LESSP instead of ALPHALESSP.

(REMPROP 'EXPLODE       'COMPILER:STYLE-CHECKER)
(REMPROP 'EXPLODEC      'COMPILER:STYLE-CHECKER)
(REMPROP 'EXPLODEN      'COMPILER:STYLE-CHECKER)
(REMPROP 'ALPHALESSP    'COMPILER:STYLE-CHECKER)
(REMPROP 'GETCHARN      'COMPILER:STYLE-CHECKER)
(REMPROP 'GETCHAR       'COMPILER:STYLE-CHECKER)
(REMPROP 'IMPLODE       'COMPILER:STYLE-CHECKER)
(REMPROP 'MAKNAM        'COMPILER:STYLE-CHECKER)

(DEFMACRO SFA-CALL (STREAM OPERATION ARG)
  `(FUNCALL ,STREAM (READ-FROM-STRING (STRING-APPEND #/: ,OPERATION)) ,ARG))

;; Things that appear within DECLARE bodies.

;; Why doesn't this work?
;; Because of the brain-damaged way the lispm compiler is written. -gjc
;; (PUTPROP 'DECLARE '(DECLARE-OPTIMIZER) 'COMPILER:OPTIMIZERS)
;;
;; (DEFUN DECLARE-OPTIMIZER (DECLARE-FORM &AUX (RETURN-FORM NIL))
;;   (DO ((L (REVERSE (CDR DECLARE-FORM)) (CDR L)))
;;       ((NULL L))
;;     (IF (NOT (MEMQ (CAAR L)
;; 		   '(FIXNUM FLONUM NOTYPE MACROS ARRAY* GENPREFIX
;; 			    CLOSED MUZZLED MAPEX SPLITFILE)))
;; 	(PUSH (CAR L) RETURN-FORM)))
;;   (IF RETURN-FORM (CONS 'DECLARE RETURN-FORM) NIL))

;; These are in global, so avoid redefinition warning by using FDEFINE
;; rather than DEFUN.
(FDEFINE 'FLONUM #'(LAMBDA (&QUOTE &REST IGNORE) NIL))
(FDEFINE 'FIXNUM #'(LAMBDA (&QUOTE &REST IGNORE) NIL))
(FDEFINE 'ARGS	 #'(LAMBDA (&QUOTE &REST IGNORE) NIL))

(DEFUN MACROS	     (&QUOTE &REST IGNORE) NIL)
(DEFUN CLOSED	     (&QUOTE &REST IGNORE) NIL)
(DEFUN NOTYPE	     (&QUOTE &REST IGNORE) NIL)
(DEFUN ARRAY*	     (&QUOTE &REST IGNORE) NIL)
(DEFUN GENPREFIX     (&QUOTE &REST IGNORE) NIL)
(DEFUN MUZZLED	     (&QUOTE &REST IGNORE) NIL)
(DEFUN MAPEX	     (&QUOTE &REST IGNORE) NIL)
(DEFUN SPLITFILE     (&QUOTE &REST IGNORE) NIL)
(DEFUN EXPR-HASH     (&QUOTE &REST IGNORE) NIL)

;; Run time stuff

(DEFUN SYMBOLCONC (&REST SYMS)
  (INTERN (APPLY #'STRING-APPEND
		 (MAPCAR #'(LAMBDA (SYM)
			     (COND ((FLOATP SYM)
				    (FORMAT NIL "~S" SYM))
				   ((FIXP SYM)
				    (FORMAT NIL "~D" SYM))
				   (T SYM)))
			 SYMS))))

(DEFUN QUOTED-ARGS (&QUOTE &REST L)
  (MAPCAR #'(LAMBDA (X) (PUTPROP X '((1005 (FEF-ARG-OPT FEF-QT-QT))) 'ARGDESC))
	  L))

;; LISPM doesn't have subrcall
#+LISPM
(defmacro subrcall (type subrobj &rest args)
  (declare (ignore type))
  `(funcall ,subrobj ,@args))

#+LISPM
(defvar defaultf (fs:default-pathname))

#+LISPM
(defmacro defaultf (filespec)
  `(setq defaultf (fs:merge-pathname-defaults ,filespec)))

#+LISPM
(defmacro lengthf (filespec)
  `(with-open-file (stream ,filespec)
     (send stream :length)))

#+LISPM
(defmacro maknum (q)
 `(%pointer ,q))

#+LISPM
(DEFMACRO macsyma-store (ARRAY-REFERENCE VALUE)
  (LET* ((ARRAYCALL (MACROEXPAND ARRAY-REFERENCE)))
    (SELECTQ (CAR ARRAYCALL)
      (FUNCALL `(ASET ,VALUE (symbol-function ,(CADR ARRAYCALL)) . ,(CDDR ARRAYCALL)))
      (ARRAYCALL
         `(ASET ,VALUE ,(CADDR ARRAYCALL) . ,(CDDDR ARRAYCALL))
      )
      ((APPLY FUNCALL* APPLY)
       `(APPLY 'ASET ,VALUE ,(CADR ARRAYCALL) . ,(CDDR ARRAYCALL)))
      (T `(ASET ,VALUE (FUNCTION ,(CAR ARRAYCALL)) . ,(CDR ARRAYCALL))))))


#+3600
(PROGN 'COMPILE

;;; On the 3600, STORE isn't implemented.  So, implement enough of
;;; it here to satisfy the cases the Macsyma uses.  I have yet to find
;;; it using complicated side effects of the array reference -- it's either
;;; a (STORE (ARRAYCALL ...) ...) or a (STORE (FUNCALL ...) ...) or else
;;; a (STORE (array-called-as-function ...) ...).  So, assume that if the CAR
;;; of the first form isn't ARRAYCALL or FUNCALL, then it's a STORE of the third
;;; form.

(DEFUN STORE-MACRO-HELPER (ARRAY-REF NEW-VALUE)
  (SELECTQ (LENGTH ARRAY-REF)
    (2 `(STORE-INTERNAL-1D ,@ARRAY-REF ,NEW-VALUE))
    (3 `(STORE-INTERNAL-2D ,@ARRAY-REF ,NEW-VALUE))
    (OTHERWISE (FERROR "Cannot expand STORE for array reference ~S" ARRAY-REF))))

(DEFMACRO STORE (ARRAY-REF NEW-VALUE)
  (SETQ ARRAY-REF (MACROEXPAND ARRAY-REF))	;Maybe something that expands right.
  (SELECTQ (FIRST ARRAY-REF)
    (FUNCALL (STORE-MACRO-HELPER (CDR ARRAY-REF) NEW-VALUE))
    (ARRAYCALL (STORE-MACRO-HELPER (CDDR ARRAY-REF) NEW-VALUE))
    (OTHERWISE (STORE-MACRO-HELPER `(#',(FIRST ARRAY-REF) . ,(CDR ARRAY-REF)) NEW-VALUE))))


(DEFUN STORE-INTERNAL-1D (ARRAY-SPEC INDEX NEW-VALUE)
  (LOOP UNTIL (ARRAYP ARRAY-SPEC)
	DO (COND ((SYMBOLP ARRAY-SPEC) (SETQ ARRAY-SPEC (FSYMEVAL ARRAY-SPEC)))
		 (T (FERROR "STORE failed -- can't find array for ~S" ARRAY-SPEC))))
  (SETF (AREF ARRAY-SPEC INDEX) NEW-VALUE))

(DEFUN STORE-INTERNAL-2D (ARRAY-SPEC I1 I2 NEW-VALUE)
  (LOOP UNTIL (ARRAYP ARRAY-SPEC)
	DO (COND ((SYMBOLP ARRAY-SPEC) (SETQ ARRAY-SPEC (FSYMEVAL ARRAY-SPEC)))
		 (T (FERROR "STORE failed -- can't find array for ~S" ARRAY-SPEC))))
  (SETF (AREF ARRAY-SPEC I1 I2) NEW-VALUE))

)  ;End PROGN 'COMPILE









