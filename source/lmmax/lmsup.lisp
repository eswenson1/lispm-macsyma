;;; -*- Mode:LISP; Package:MACSYMA; Base:10 -*-

;; -*- Mode: Lisp; Package: Macsyma -*-

;; (c) Copyright 1983 Massachusetts Institute of Technology

;; Lisp Machine additions to the Macsyma suprvisor -- see 
;; MAXII;SYSTEM > and JPG;SUPRV > for the remaining gory details.

(declare (special $grind)
         (*expr mread filename-from-arg-list continue supunbind 
                more-fun-internal))

(DEFFLAVOR MACSYMA-LISTENER ()
	   (TV:NOTIFICATION-MIXIN TV:PROCESS-MIXIN TV:FULL-SCREEN-HACK-MIXIN TV:WINDOW)
  (:DEFAULT-INIT-PLIST :SAVE-BITS T		
		       :FONT-MAP '(FONTS:CPTFONT FONTS:CPTFONTB)
		       :PROCESS '(MACSYMA-LISTEN-LOOP :REGULAR-PDL-SIZE 40000
						      :SPECIAL-PDL-SIZE 5000))
  (:DOCUMENTATION :COMBINATION "Normal MACSYMA window"))

;;; This code tries to make Macsyma deal with Lispm more exceptions correctly.
;;; You should talk to me if you think it needs tweaking, please! --HIC

(DEFMETHOD (MACSYMA-LISTENER :MORE-EXCEPTION) ()
  (if (null tv:more-vpos)
      (setq tv:more-vpos 100000))
  (TV:SHEET-MORE-HANDLER ':MACSYMA-MORE NIL))

(DEFMETHOD (MACSYMA-LISTENER :MACSYMA-MORE) ()
  (if (null tv:more-vpos)
      (setq tv:more-vpos 100000))
  (MORE-FUN-INTERNAL SELF))

(DEFMETHOD (MACSYMA-LISTENER :DEFER-MORE) ()
  (OR (NULL TV:MORE-VPOS)			
      ( TV:MORE-VPOS 100000)
      (INCF TV:MORE-VPOS 100000)))

;; When changing the size or fonts of a window, update the variables DISPLA looks at.  It
;; would probably be better to make all of the DISPLA variables special instance
;; variables.  These would be placed inside a listener object, so that they would be
;; accessible from both the Macsyma listener window and the supdup server.

;; Check that PROCESS is really a process, since this gets called when the :INIT
;; method is run, which is before the process has been created.

(DEFUN-METHOD REBIND-SPECIALS MACSYMA-LISTENER (&REST SPECIALS)
  (LET ((PROCESS (SEND SELF ':PROCESS)))
    (IF (TYPEP PROCESS 'SI:PROCESS)
	(WITHOUT-INTERRUPTS
	  (LOOP WITH STACK-GROUP = (SEND PROCESS ':STACK-GROUP)
		FOR (SPECIAL VALUE) ON SPECIALS BY 'CDDR DO
		;; Though this dual code shouldn't be necessary, on the 3600
		;; it seems that it is.  Anyway, it's probably the right thing
		;; to do.  --HIC
		(IF (EQ PROCESS CURRENT-PROCESS)
		    (SET SPECIAL VALUE)
		    ;(DBG:REBIND-IN-STACK-GROUP SPECIAL VALUE STACK-GROUP)
		    (eh:set-in-stack-group special stack-group value)))))))

(DEFUN-METHOD REBIND-SIZE-SPECIALS MACSYMA-LISTENER (&REST IGNORE)
  (MULTIPLE-VALUE-BIND (X Y)
      (SEND SELF ':SIZE-IN-CHARACTERS)
    (REBIND-SPECIALS 'LINEL X 'TTYHEIGHT Y)))

(DEFMETHOD (MACSYMA-LISTENER :AFTER :CHANGE-OF-SIZE-OR-MARGINS) REBIND-SIZE-SPECIALS)

(DEFUN-METHOD REBIND-FONT-SPECIALS MACSYMA-LISTENER (&REST IGNORE)
  (REBIND-SIZE-SPECIALS)
  (REBIND-SPECIALS 'LG-CHARACTER-X TV:CHAR-WIDTH
		   'LG-CHARACTER-Y TV:LINE-HEIGHT
		   'LG-CHARACTER-X-2 (// TV:CHAR-WIDTH 2)
		   'LG-CHARACTER-Y-2 (// TV:LINE-HEIGHT 2))
  (LET ((BLINKER (TV:SHEET-FOLLOWING-BLINKER SELF)))
    (AND BLINKER
	 (SEND BLINKER ':SET-SIZE (FONT-BLINKER-WIDTH TV:CURRENT-FONT)
				  (FONT-BLINKER-HEIGHT TV:CURRENT-FONT)))))

(DEFMETHOD (MACSYMA-LISTENER :AFTER :SET-CURRENT-FONT) REBIND-FONT-SPECIALS)
(DEFMETHOD (MACSYMA-LISTENER :AFTER :SET-FONT-MAP) REBIND-FONT-SPECIALS)

(COMPILE-FLAVOR-METHODS MACSYMA-LISTENER)

;; The top level function for Macsyma windows.  The :KILL operation resets
;; the process and buries the window.  MACSYMA-TOP-LEVEL exits when the user
;; types QUIT();.  Bind TERMINAL-IO here rather than in MACSYMA-TOP-LEVEL since
;; it is already bound in the supdup server.

(DEFUN MACSYMA-LISTEN-LOOP (TERMINAL-IO)
  (LOOP DO
	(MACSYMA-TOP-LEVEL)
	(SEND TERMINAL-IO ':BURY)))

;; Typing (MACSYMA) causes the MACSYMA-TOP-WINDOW to be selected if typed from the Lisp
;; Machine keyboard (TERMINAL-IO is a window stream).  If typed from some other stream,
;; just enter the normal read-eval-print loop.  MACSYMA-TOP-WINDOW is analgous to
;; TV:INITIAL-LISP-LISTENER.

(DEFVAR MACSYMA-TOP-WINDOW (TV:MAKE-WINDOW 'MACSYMA-LISTENER))

(DEFUN FIND-MACSYMA-TOP-WINDOW ()
  (IF (NULL MACSYMA-TOP-WINDOW)
      (SETQ MACSYMA-TOP-WINDOW (TV:MAKE-WINDOW 'MACSYMA-LISTENER))
      MACSYMA-TOP-WINDOW))

(DEFUN MACSYMA ()
  (IF (TYPEP TERMINAL-IO 'TV:SHEET)
      (SEND (FIND-MACSYMA-TOP-WINDOW) ':SELECT)
      (MACSYMA-TOP-LEVEL)))

;; SMART-TTY and LINE-GRAHPICS-TTY are used by MRG;DISPLA and are set up on ITS in
;; ALJABR;LOADER.  RUBOUT-TTY is used by SUPRV and can be flushed when we start using the
;; Lisp Machine editor.  SCROLLP and SMART-TTY are equivalent for our purposes.

(DECLARE (SPECIAL SMART-TTY RUBOUT-TTY LINE-GRAPHICS-TTY CHARACTER-GRAPHICS-TTY
		  LINEL TTYHEIGHT SCROLLP LG-OLD-X LG-OLD-Y
		  LG-CHARACTER-X LG-CHARACTER-Y LG-CHARACTER-X-2 LG-CHARACTER-Y-2))

(DEFUN MACSYMA-TOP-LEVEL ()
  (LET* ((STANDARD-OUTPUT #'MACSYMA-OUTPUT)
	 (^R NIL) (^W NIL)
	 (PACKAGE (PKG-FIND-PACKAGE "MACSYMA"))
	 (BASE 10.) (IBASE 10.) (*NOPOINT T)
	 (W-O (FUNCALL TERMINAL-IO ':WHICH-OPERATIONS))
	 ;; Bind for multiple instantiations -- these variables
	 ;; are stream-dependent.
	 (SMART-TTY (MEMQ ':SET-CURSORPOS W-O))
	 (RUBOUT-TTY SMART-TTY)
	 (SCROLLP (NOT SMART-TTY))
	 (LINE-GRAPHICS-TTY (MEMQ ':DRAW-LINE W-O))
	 ;; Bind for multiple instantiations -- these variables are stream-dependent.
	 (SMART-TTY (SEND TERMINAL-IO ':OPERATION-HANDLED-P ':SET-CURSORPOS))
	 (RUBOUT-TTY SMART-TTY)
	 (SCROLLP (NOT SMART-TTY))
	 (LINE-GRAPHICS-TTY (SEND TERMINAL-IO ':OPERATION-HANDLED-P ':DRAW-LINE))
	 (LINEL) (TTYHEIGHT) (LG-OLD-X) (LG-OLD-Y)
	 (LG-CHARACTER-X) (LG-CHARACTER-Y) (LG-CHARACTER-X-2) (LG-CHARACTER-Y-2))
    ;; Uncomment this when somebody tries to take car of a number again
    ;; (SET-ERROR-MODE 1 1 1 1)
    ;; What happens to height on printing ttys?
    (MULTIPLE-VALUE (LINEL TTYHEIGHT) (SEND TERMINAL-IO ':SIZE-IN-CHARACTERS))
    (COND (LINE-GRAPHICS-TTY
	   (SETQ LG-CHARACTER-X (SEND TERMINAL-IO ':CHAR-WIDTH))
	   (SETQ LG-CHARACTER-Y (SEND TERMINAL-IO ':LINE-HEIGHT))
	   (SETQ LG-CHARACTER-X-2 (// LG-CHARACTER-X 2))
	   (SETQ LG-CHARACTER-Y-2 (// LG-CHARACTER-Y 2))))
    (PRINT-MACSYMA-COPYRIGHT TERMINAL-IO)
    (*CATCH 'MACSYMA-QUIT
      (ERROR-RESTART-LOOP ((SYS:ABORT) "Macsyma Top Level~@[ in ~A~]"
			   (SEND TERMINAL-IO ':SEND-IF-HANDLES ':NAME))
	(UNWIND-PROTECT
	  (CONDITION-CASE ()
	      (SEND TERMINAL-iO ':FUNCALL-INSIDE-YOURSELF #'CONTINUE)
	    (MACSYMA-ERROR))
	  (SUPUNBIND))))))

;; Add "Macsyma" to the window creation menu and System key.
(PUSH '("Macsyma" :VALUE MACSYMA-LISTENER
	:DOCUMENTATION "Macsyma Symbolic Algebra System")
      TV:DEFAULT-WINDOW-TYPES-ITEM-LIST)

(PUSH '(#/A MACSYMA-LISTENER "Macsyma" T) TV:*SYSTEM-KEYS*)


;; Print out the Macsyma Copyright notice to the appropriate window
(DEFUN PRINT-MACSYMA-COPYRIGHT (WINDOW)
  (MULTIPLE-VALUE-BIND (MAJOR MINOR) (SI:GET-SYSTEM-VERSION 'MACSYMA)
    (FORMAT WINDOW "~&~%This is Macsyma ~D.~D" MAJOR MINOR))
  (FORMAT WINDOW "~&(c) Copyright 1976, 1983 Massachusetts Institute of Technology~2%"))

;; Random garbage needed to make SUPRV happy.

(DEFUN FORMFEED () (SEND STANDARD-INPUT ':CLEAR-SCREEN))

;; This is used someplace in SUPRV.

(DEFUN FILE-OPEN (FILE-OBJ)
  (EQ (SEND FILE-OBJ ':STATUS) ':OPEN))

;; Takes a string file specification and returns an oldio list specification.  Similar
;; to MacLisp NAMELIST function. (UNEXPAND-PATHNAME "C: D; A B") --> (A B C D)

(DEFUN UNEXPAND-PATHNAME (STRING)
  (LET* ((PATHNAME (FS:PARSE-PATHNAME STRING))
	 (DEV (SEND PATHNAME ':DEVICE)))
    (IF (STRING-EQUAL DEV "DSK")
	(SETQ DEV (SEND PATHNAME ':HOST)))
    (MAPCAR 'INTERN (LIST (SEND PATHNAME ':FN1)
			  (SEND PATHNAME ':FN2)
			  (SEND DEV ':NAME-AS-FILE-COMPUTER)
			  (SEND PATHNAME ':DIRECTORY)))))

;;; Make this function callable on different types of things.
(DEFUN NAMESTRING (OLDIO-LIST)
  (cond ((symbolp oldio-list) (string oldio-list))
	((stringp oldio-list) oldio-list)
	(t (INTERN
	     (FORMAT NIL "~A/:~A/;~A ~A"
		     (THIRD OLDIO-LIST)
		     (FOURTH OLDIO-LIST)
		     (FIRST OLDIO-LIST)
		     (SECOND OLDIO-LIST))))))

;; Takes a list like in ITS Macsyma, returns a string.
;; Device defaults to MC, as does DSK device specification.
;; Directory defaults to user-id.  Hack USERSn later.
;; (FILESTRIP '($A $B $C $D)) --> "C: D; A B"
;; (FILESTRIP '($A $B $C)) --> "MC: C; A B"

(DEFUN FILESTRIP (X &AUX FN1 FN2 DEV DIR)
  (IF (AND (= (LENGTH X) 1) (SYMBOLP (CAR X)) (= (AREF (STRING (CAR X)) 0) #/&))
      ;; A Macsyma string, use it as is
      (SUBSTRING (STRING (CAR X)) 1)
      ;; Otherwise...
      (SETQ X (FULLSTRIP X)) ;Strip the list, leave NIL as NIL.
      (SETQ FN1 (CAR X) FN2 (CADR X) DEV (CADDR X) DIR (CADDDR X))
      (IF (AND DEV (NULL DIR)) (SETQ DIR DEV DEV NIL))
      (IF (EQ DEV 'DSK) (SETQ DEV "MC"))
      ;;If case doesn't matter, don't confuse user.
      (STRING-UPCASE (FORMAT NIL "~A: ~A; ~A ~A"
			      (OR DEV "MC") (OR DIR USER-ID)
			      (OR FN1 "MAXOUT") (OR FN2 ">")))))

(DEFUN FIND (FUNC MEXPRP)
  MEXPRP
  (COND ((GET FUNC 'AUTOLOAD)
	 (FERROR NIL "~A is needed from file ~S, but not in core"
		 FUNC (GET FUNC 'AUTOLOAD))))
  NIL)

;(DECLARE (SPECIAL ERROR-CALL))
;(DEFUN MACSYMA-ERROR-HANDLER (&REST IGNORE)
;   (COND ((NULL ERROR-CALL) NIL)
;	 (T (LET ((SIGNAL))
;		 (SETQ SIGNAL (FUNCALL ERROR-CALL NIL))
;		 (COND ((NULL SIGNAL) NIL)
;		       ((EQ SIGNAL 'LISP)
;			(SETQ EH:ERRSET-STATUS NIL
;			      EH:ERRSET-PRINT-MSG T)
;			NIL)
;		       ((EQ SIGNAL 'EXIT) (*THROW 'SI:TOP-LEVEL NIL))
;		       (T NIL))))))

(DEFUN TOP-MEVAL (FORM)
  (CATCH-ERROR-RESTART (SYS:ABORT "TOP-MEVAL")
    (NCONS (MEVAL* FORM))))

(DECLARE (SPECIAL WRITEFILE-OUTPUT))
(DECLARE (SPECIAL WRITEFILE-OPERATIONS))
(DEFVAR ^R NIL)
(DEFVAR ^W NIL)
(DEFVAR INFILE NIL)

;;; STANDARD-OUTPUT gets bound to this.
(DEFUN MACSYMA-OUTPUT (OP &REST REST)
  (SELECTQ OP
    (:WHICH-OPERATIONS (SEND TERMINAL-IO ':WHICH-OPERATIONS))
    (T (IF (AND ^R (MEMQ OP WRITEFILE-OPERATIONS))
	   (LEXPR-FUNCALL WRITEFILE-OUTPUT OP REST))
       (IF (NOT ^W) (LEXPR-FUNCALL TERMINAL-IO OP REST)))))

;; Specify entire filename when WRITEFILE is done.
(DEFMFUN $WRITEFILE (&REST L)
  (LET ((NAME ($FILENAME_MERGE (FILENAME-FROM-ARG-LIST L)
			       "MAXOUT"
			       (FS:USER-HOMEDIR))))
  (SETQ WRITEFILE-OUTPUT (OPEN NAME ':OUT))
  (SETQ WRITEFILE-OPERATIONS (SEND WRITEFILE-OUTPUT ':WHICH-OPERATIONS))
  (SETQ ^R T)
  NAME))

(DEFUN $CLOSEFILE ()
  (SETQ ^R NIL)
  (CLOSE WRITEFILE-OUTPUT)
  '$DONE)

;; Random useful functions to call from Macsyma Toplevel.

(DEFF $ED #'ED)
(DEFF $BEEP #'TV:BEEP)

#-3600
(DEFF $GC_ON #'GC-ON)
#-3600
(DEFF $GC_OFF #'GC-OFF)

(DEFUN $SCHEDULE (&OPTIONAL (N 1)) (DOTIMES (I N) (PROCESS-ALLOW-SCHEDULE)))
(DEFUN $SLEEP (&OPTIONAL (60THS-SEC 60.)) (PROCESS-SLEEP 60THS-SEC))

(DEFMVAR EDITOR-STATE NIL "Alist of editor windows and old strings, one per Macsyma Listener")

;;; Edit a frob, then read it back in
(DEFMSPEC $EDIT (X)
  (SETQ X (SECOND X))
  ;; Convert Macsyma expression into a string
  (AND X
       (LET (($GRIND T))
	 (SETQ X (MAPPLY '$STRING (LIST X) '$STRING))))
  (LET ((STATE (ASSQ TERMINAL-IO EDITOR-STATE))
	(WINDOW))
    (COND ((NULL STATE)
	   (SETQ STATE (LIST TERMINAL-IO NIL ""))
	   (PUSH STATE EDITOR-STATE)))
    (LET ((TV:DEFAULT-SCREEN TV:MOUSE-SHEET))
      (MULTIPLE-VALUE (X WINDOW) (ZWEI:EDSTRING (IF X (NSUBSTRING (STRING X) 1) (THIRD STATE))
						(SECOND STATE))))
    (SETF (SECOND STATE) WINDOW)
    (SETF (THIRD STATE) X)
    (WITH-INPUT-FROM-STRING (STREAM (STRING-APPEND X ";"))
      (MEVAL* (THIRD (MREAD STREAM))))))

;; To do:
;; Figure out some way of making $LINENUM and $% process-specific.
;; JLK suggests something like D4.23 as meaning D-line 23 in Macsyma Listener #4.
;; Make Macsyma Windows into scroll windows.
