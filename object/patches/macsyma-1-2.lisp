;;; -*- Mode:Lisp; Readtable:ZL; Package:MACSYMA; Base:10; Patch-File:T -*-
;;; Patch file for Macsyma version 1.2
;;; Reason:
;;;  Fixes issue with UNORDER macsyma function, which was calling REMALIAS with the wrong
;;; Reason:
;;;  number of arguments.
;;; Written 27-Apr-23 15:00:18 by ESWENSON,
;;; while running on Lisp Machine One from band 3
;;; with Experimental System 100.6, Experimental Macsyma 1.1, microcode 323.



; From file OZ: /mnt/its/lisp_machines/ws/cadr/l/sys/macsyma/source/jpg/comm2.lisp at 27-Apr-23 15:00:18
#10R MACSYMA#:
(COMPILER-LET ((*PACKAGE* (PKG-FIND-PACKAGE "MACSYMA")))
  (COMPILER::PATCH-SOURCE-FILE "OZ: //tree//macsyma//source//jpg//comm2"

(DEFMFUN $UNORDER NIL
 (flet ((do-remalias (x)
           (remalias x nil)))
   (LET ((L (DELQ NIL
		(CONS '(MLIST SIMP)
		      (NCONC (MAPCAR #'do-remalias (MAPCAR #'GETALIAS LESSORDER))
			     (MAPCAR #'do-remalias (MAPCAR #'GETALIAS GREATORDER)))))))
      (SETQ LESSORDER NIL GREATORDER NIL) L)))
))
