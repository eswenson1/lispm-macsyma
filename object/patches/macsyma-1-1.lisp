;;; -*- Mode:Lisp; Readtable:ZL; Package:MACSYMA; Base:10; Patch-File:T -*-
;;; Patch file for Macsyma version 1.1
;;; Reason:
;;;  Another fix to pathname handling in macsyma.
;;; Reason:
;;;  Fix issue with filename parsing for batch/demo support.
;;; Reason:
;;;  Fix issue with RITEDIV not handling gensym arrays.
;;; Written 21-Apr-23 19:38:01 by eswenson,
;;; while running on Lisp Machine One from band 8
;;; with Experimental System 100.5, Experimental Macsyma 1.0, microcode 323.



; From file OZ: /mnt/its/lisp_machines/ws/cadr/l/sys/macsyma/source/paulw/matrix.lisp at 21-Apr-23 19:38:28
#10R MACSYMA#:
(COMPILER-LET ((*PACKAGE* (PKG-FIND-PACKAGE "MACSYMA")))
  (COMPILER::PATCH-SOURCE-FILE "OZ: //tree//macsyma//source//paulw//matrix"

(DEFUN RITEDIV (X M N A)
  (DECLARE(FIXNUM J I M N))
  (PROG (J I D ERRRJFFLAG)
	(SETQ ERRRJFFLAG T)
	(SETQ I M)
  LOOP1 (COND ((ZEROP I) (RETURN NIL)))
	(macsyma-STORE (FUNCALL X I I) NIL)
	(SETQ J M)
   LOOP (COND ((= J N) (SETQ I (1- I)) (GO LOOP1)))
	(SETQ J (1+ J))
	(COND ((EQUAL A 1)
	       (macsyma-STORE (FUNCALL X I J) (CONS (FUNCALL X I J) 1))
	       (GO LOOP)))
	(SETQ D (*CATCH 'RATERR (PQUOTIENT (FUNCALL X I J) A)))
	(SETQ D (COND (D (CONS D 1)) (T (RATREDUCE (FUNCALL X I J) A))))
	(macsyma-STORE (FUNCALL X I J) D)
	(GO LOOP)))

))

; From file OZ: /mnt/its/lisp_machines/ws/cadr/l/sys/macsyma/source/maxsrc/mload.lisp at 21-Apr-23 19:44:00
#10R MACSYMA#:
(COMPILER-LET ((*PACKAGE* (PKG-FIND-PACKAGE "MACSYMA")))
  (COMPILER::PATCH-SOURCE-FILE "OZ: //tree//macsyma//source//maxsrc//mload"

(DEFUN MACSYMA-NAMESTRING-SUB (USER-OBJECT)
  (IF (MACSYMA-NAMESTRINGP USER-OBJECT) USER-OBJECT
      (LET* ((SYSTEM-OBJECT
	       (COND ((ATOM USER-OBJECT)
		      (FULLSTRIP1 USER-OBJECT))
		     (($LISTP USER-OBJECT)
		      (namestring (FULLSTRIP (CDR USER-OBJECT))))
		     (T USER-OBJECT))))
	(STRING SYSTEM-OBJECT))))
))

; From file OZ: /mnt/its/lisp_machines/ws/cadr/l/sys/macsyma/source/maxsrc/mload.lisp at 21-Apr-23 20:32:21
#10R MACSYMA#:
(COMPILER-LET ((*PACKAGE* (PKG-FIND-PACKAGE "MACSYMA")))
  (COMPILER::PATCH-SOURCE-FILE "OZ: //tree//macsyma//source//maxsrc//mload"

(DEFMFUN $FILENAME_MERGE (&REST FILE-SPECS)
  (DO ((SPECS FILE-SPECS (CDR SPECS))
       (F "" (FS:MERGE-PATHNAME-defaults F (MACSYMA-NAMESTRING-SUB (CAR SPECS)))))
      ((NULL SPECS)
       (TO-MACSYMA-NAMESTRING F))))

))
