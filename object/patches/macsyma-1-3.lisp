;;; -*- Mode:Lisp; Readtable:ZL; Package:MACSYMA; Base:10; Patch-File:T -*-
;;; Patch file for Macsyma version 1.3
;;; Reason:
;;;  Fix end of batch file handling. Fix more handling.
;;; Written 27-Apr-23 17:37:07 by ESWENSON,
;;; while running on Lisp Machine One from band 3
;;; with Experimental System 100.6, Experimental Macsyma 1.2, microcode 323.



; From file OZ: /mnt/its/lisp_machines/ws/cadr/l/sys/macsyma/source/maxii/system.lisp at 27-Apr-23 17:37:08
#10R MACSYMA#:
(COMPILER-LET ((*PACKAGE* (PKG-FIND-PACKAGE "MACSYMA")))
  (COMPILER::PATCH-SOURCE-FILE "OZ: //tree//macsyma//source//maxii//system"

(DEFUN CONTINUE (&OPTIONAL (STANDARD-INPUT STANDARD-INPUT) BATCH-OR-DEMO-FLAG)
  (DO ((R)
       (time-before) (time-after) (time-used)
       (EOF (LIST NIL)))
      (NIL)
    (LET (((C-TAG D-TAG)
	   (MAKE-LINE-LABELS $INCHAR $OUTCHAR)))
      (SETQ R (LET ((*MREAD-PROMPT* (MAIN-PROMPT)))
		(AND BATCH-OR-DEMO-FLAG
		     (FORMAT STANDARD-OUTPUT "~%~A" *MREAD-PROMPT*))
		(MREAD STANDARD-INPUT EOF)))
      (IF (or (EQ R EOF) (null r)) (RETURN '$DONE))
      (TERPRI STANDARD-INPUT)
      (FUNCALL STANDARD-INPUT ':FORCE-OUTPUT)
      (SETQ $_ (CADDR R))
      (SET  C-TAG $_)
      (SETQ TIME-BEFORE (TIME))
      (SETQ $% (TOPLEVEL-MACSYMA-EVAL $_))
      (SETQ TIME-AFTER (TIME))
      (SETQ TIME-USED (* 1000.0 (QUOTIENT (TIME-DIFFERENCE TIME-AFTER TIME-BEFORE)
					  60.0)))
      (SETQ ACCUMULATED-TIME (PLUS ACCUMULATED-TIME TIME-USED))
      (SET  D-TAG $%)
      (SETQ $__ $_)
      (PUTPROP D-TAG `(,TIME-USED . 0) 'TIME)
      (if $SHOWTIME
	  (FORMAT STANDARD-OUTPUT "~&Time= ~3F msecs.~&" time-used))
      (IF (EQ (CAAR R) 'DISPLAYINPUT)
	  (DISPLA `((MLABLE) ,D-TAG ,$%))
	  (PROGN (PUTPROP C-TAG T 'NODISP)
		 (PUTPROP D-TAG T 'NODISP)))
      (COND ((EQ BATCH-OR-DEMO-FLAG ':DEMO)
	     (MTELL (if $LONG_DEMO_MESSAGE
			"~&Pausing.  Type any character to continue demo.~%"
			"~&_"))
	     (IF (AND (MEMQ (FUNCALL STANDARD-OUTPUT ':TYI) '(#\CLEAR-SCREEN #^L))
		      (MEMQ ':CLEAR-SCREEN (FUNCALL STANDARD-OUTPUT ':WHICH-OPERATIONS)))
		 (FUNCALL STANDARD-OUTPUT ':CLEAR-SCREEN)
		 (TERPRI STANDARD-OUTPUT))))
      ;; This is sort of a kludge -- eat newlines and blanks so that they don't echo
      (AND BATCH-OR-DEMO-FLAG
	   (MEMQ ':TYI-NO-ECHO (FUNCALL STANDARD-INPUT ':WHICH-OPERATIONS))
	   (DO (CHAR) (())
	     (SETQ CHAR (FUNCALL STANDARD-INPUT ':TYI-NO-ECHO))
	     (COND ((NOT (OR (= CHAR #\SPACE) (= CHAR #\NEWLINE) (= CHAR #\TAB)))
		    (FUNCALL STANDARD-INPUT ':UNTYI-NO-ECHO CHAR)
		    (RETURN))))))))
))

; From file OZ: /mnt/its/lisp_machines/ws/cadr/l/sys/macsyma/source/lmmax/lmsup.lisp at 27-Apr-23 17:37:59
#10R MACSYMA#:
(COMPILER-LET ((*PACKAGE* (PKG-FIND-PACKAGE "MACSYMA")))
  (COMPILER::PATCH-SOURCE-FILE "OZ: //tree//macsyma//source//lmmax//lmsup"

(DEFMETHOD (MACSYMA-LISTENER :MORE-EXCEPTION) ()
  (if (null tv:more-vpos)
      (setq tv:more-vpos 100000))
  (TV:SHEET-MORE-HANDLER ':MACSYMA-MORE NIL))

(DEFMETHOD (MACSYMA-LISTENER :MACSYMA-MORE) ()
  (if (null tv:more-vpos)
      (setq tv:more-vpos 100000))
  (MORE-FUN-INTERNAL SELF))

))
